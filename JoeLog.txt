
Joe Mingoia
CSE306 EXP02
Group: Lazy Steel Desperados

***********************************************************************************************
Date: 04/19/18
Time: 6:30pm - 9:00pm

Members Present:
                Joe
                Doug
		Dylan

Tools used:
        Command Line
        Timberlake
        Git
        Bitbucket
	Online RGB Color Creator
	GIMP
	Trello
	Pen and Paper	
	GDB

Activity: Selected a pixel from original image, and chose the same pixel from the updated image and compared RGB amounts, as well as converting the hex color value to binary to compare what bits changed states.
	-Did this in order to establish possible trends and better understand what is occuring.
	
	-This established that the code is changing a more significant bit/multiple bits more than it should be, instead of only changing the least significant bit.

Created Trello variable list to better keep track of the variables in the program.

Made multiple code changes and reverted back to original as shown by Dylan's Commits.

Have better understanding on how code is functioning.






************************************************************************************************
Date: 04/20/18
Time: 2:30pm
Members Present:
				Joe
Tools used:
        Command Line
        Timberlake
        Git
        Bitbucket
	Online RGB Color Creator
	GIMP
	Trello
	Pen and Paper	
	GDB
Activity: We have realized where the image is turning blue.
		Realized that there is an error within the offset, and it is not what it should be
